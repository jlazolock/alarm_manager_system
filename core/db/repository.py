import datetime
import os
from enum import Enum

from core.db.connection import MySqlConnector, QueryResult, OracleConnector


class BestEnum(Enum):
    @classmethod
    def choices(cls):
        return tuple((x.value, x.name) for x in cls)


class EstadoGenericoEnum(BestEnum):
    ACTIVO = 'ACT'
    INACTIVO = 'DES'


class EstadoNeEnum(BestEnum):
    ON_AIR = 1
    OFF_AIR = 2


class PrioridadNodoEnum(BestEnum):
    P0mas = 'P0+'
    P0 = 'P0'
    P1 = 'P1'
    P2 = 'P2'
    P3 = 'P3'
    P4 = 'P4'


class PrioridadTicketEnum(BestEnum):
    BAJA = 1
    MEDIA = 2
    ALTA = 3
    CRITICA = 4


class MySqlOymConnector(MySqlConnector):

    def __init__(self):
        super().__init__()

    @staticmethod
    def select_proveedor_mtto_fb_idnodo(idnodo):
        '''
        :param str nombre_tipo_incidente:
        :return:
        '''
        params = {'idnodo': idnodo}

        def query(cursor, headers):
            query = '''
                    SELECT a.nombre_completo, b.proveedor_mantenimiento  FROM db_oym_inventario_general.tb_status_site a
                    left join (SELECT b.id, b.proveedor_mantenimiento FROM db_oym_inventario_general.tb_proveedor_mantenimiento b) b
                    on (a.proveedor_mantenimiento = b.id) 
                    where substr(a.nombre_completo, 1, 7) = %(idnodo)s
                    and a.estado = '2'
                '''
            cursor.execute(query, params)
            return QueryResult(result=cursor.fetchall(), headers=headers)

        return query

    @staticmethod
    def select_tb_status_site_fb_ls_idnodo(ls_idnodo):
        '''

        :param str nombrene:
        :return:
        '''
        params = {}
        default_headers = ('nombrene', 'prioridad')
        ls_idnodo = ["'{}'".format(item) for item in ls_idnodo]
        ls_idnodo = ', '.join(ls_idnodo)

        def query(cursor, headers=default_headers):
            if not ls_idnodo:
                return QueryResult(result=[], headers=headers)
            query = '''
                select (nombre_completo),
                       case
                         when prioridad = 5 then 'P0+'
                         when prioridad = 0 then 'P0'
                         when prioridad = 1 then 'P1'
                         when prioridad = 2 then 'P2'
                         when prioridad = 3 then 'P3'
                         when prioridad = 4 then 'P4'
                         end prioridad_nombre
                from tb_status_site
                where substr(nombre_completo,1,7) in ({})
                '''.format(ls_idnodo)
            cursor.execute(query, params)
            return QueryResult(result=cursor.fetchall(), headers=headers)

        return query

    @staticmethod
    def select_tb_status_site_fb_idnodo(idnodo):
        '''

        :param str idnodo:
        :return:
        '''
        params = {'idnodo': idnodo}
        default_headers = ('nombrene', 'prioridad')

        def query(cursor, headers=default_headers):
            query = '''
                select nombre_completo,
                       case
                         when prioridad = 5 then 'P0+'
                         when prioridad = 0 then 'P0'
                         when prioridad = 1 then 'P1'
                         when prioridad = 2 then 'P2'
                         when prioridad = 3 then 'P3'
                         when prioridad = 4 then 'P4'
                         end prioridad_nombre
                from tb_status_site
                where substr(nombre_completo,1,7) = %(idnodo)s
                '''
            cursor.execute(query, params)
            return QueryResult(result=cursor.fetchall(), headers=headers)

        return query

    @staticmethod
    def select_ls_proveedores_mtto_fb_ls_idnodo(ls_idnodo):
        '''

        :param list ls_idnodo: Lista de nombre de nodos
        :return:
        '''
        default_headers = ('nombrene', 'mtto')
        params = {}
        ls_idnodo = ["'{}'".format(item) for item in ls_idnodo]
        ls_idnodo = ', '.join(ls_idnodo)

        def query(cursor, headers=default_headers):
            if not ls_idnodo:
                return QueryResult(result=[], headers=headers)
            query = '''
                    SELECT distinct(a.nombre_completo) as nombrene, b.proveedor_mantenimiento as mtto
                    FROM db_oym_inventario_general.tb_status_site a
                    left join (SELECT b.id, b.proveedor_mantenimiento FROM db_oym_inventario_general.tb_proveedor_mantenimiento b) b                    
                    on (a.proveedor_mantenimiento = b.id) 
                    where substr(a.nombre_completo,1,7) in ({})
                    and a.estado = '2'
                '''.format(ls_idnodo)
            cursor.execute(query, params)
            return QueryResult(result=cursor.fetchall(), headers=headers)

        return query

    @staticmethod
    def select_tb_controlador_rectif_bancos_baterias_fb_idnodo(idnodo, limit=1):
        '''

        :param str idnodo: Nombre del nodo
        :param int limit: Limite de filas
        :return:
        '''

        params = {'idnodo': idnodo, 'limit': limit}

        def query(cursor, headers=(
                'id', 'codigo', 'nombre_completo', 'capacidad', 'prioridad', 'consumo', 'tiempo_desplazamiento',
                'tiempo_acarreo', 'tiempo_reparacion')):
            query = '''
                    select cr.id,
                           a.codigo,
                           a.nombre_completo,
                           sum(tbb.capacidad)          as capacidad,
                           a.prioridad,
                           max(ifNull((select consumo
                                       from tb_consumo_controlador_rectif
                                       where eliminado = 0
                                         and controlador_rectif = cr.id
                                       order by id desc
                                       limit 1), 0))   as Consumo,
                           max(ifNull((select tiempo_desplazamiento
                                       from tb_controlador_rectif_tiempos
                                       where controlador_rectif = cr.id
                                         and eliminado = 0
                                         and isnull(fecha_tiempo_desplazamiento) = 0
                                       order by id desc
                                       limit 1), 2))   as TiempoDesplazamiento,
                           max(ifNull((select tiempo_acarreo
                                       from tb_controlador_rectif_tiempos
                                       where controlador_rectif = cr.id
                                         and eliminado = 0
                                         and isnull(fecha_tiempo_acarreo) = 0
                                       order by id desc
                                       limit 1), 1))      TiempoAcarreo,
                           max(ifNull((select tiempo_reparacion
                                       from tb_controlador_rectif_tiempos
                                       where controlador_rectif = cr.id
                                         and fecha_tiempo_reparacion is not null
                                         and eliminado = 0
                                       order by id desc
                                       limit 1), 0.5)) as TiempoReparacion
                    from tb_status_site a
                             left join tb_controlador_rectif cr on a.id = cr.sitio
                             left join tb_energizacion_controlador_rectif ecr on ecr.id = cr.energizacion_controlador_rectif
                             left join tb_controlador_rectif_bancos_baterias crbb on crbb.controlador_rectif = cr.id
                             left join tb_tipo_bancos_baterias tbb on tbb.id = crbb.tipo_bancos_baterias
                    where substr(a.nombre_completo,1,7) = %(idnodo)s
                      and cr.energizacion_controlador_rectif = 1
                      and cr.eliminado = 0
                      and crbb.eliminado = 0
                    group by a.codigo, ecr.energizacion_controlador_rectif, a.nombre_completo, a.prioridad
                    limit %(limit)s
                '''
            cursor.execute(query, params)
            return QueryResult(result=cursor.fetchall(), headers=headers)

        return query

    @staticmethod
    def select_tb_nodos_gul_oa_fb_ls_idnodo(ls_idnodo):
        default_headers = ('codigo', 'nombre_completo', 'estado')
        params = {}

        ls_idnodo = ["'{}'".format(item) for item in ls_idnodo]
        ls_idnodo = ', '.join(ls_idnodo)

        def query(cursor, headers=default_headers):
            if not ls_idnodo:
                return QueryResult(result=[], headers=headers)

            # select substr(SITE_NAME, 1, 7) ID_NODO, SITE_NAME
            # from USAONOC.TB_NODOS_GUL_OA
            # where substr(SITE_NAME, 1, 7) in ({})
            # and OA_OYM = 'SI'

            query = """                                
                select tb_status_site.codigo, tb_status_site.nombre_completo, tb_estado.estado
                from tb_status_site
                         left join tb_estado
                                   on tb_status_site.estado = tb_estado.id
                where tb_status_site.codigo in ({})
            """.format(ls_idnodo)

            cursor.execute(query, params)
            return QueryResult(result=cursor.fetchall(), headers=headers)

        return query


# ORACLE Configuration

ORACLE_USER = os.getenv('ORACLE_USER', '')
ORACLE_PASSWORD = os.getenv('ORACLE_PASSWORD', '')
ORACLE_URL = os.getenv('ORACLE_URL', '')


class OracleTicketConnector(OracleConnector):

    def __init__(self):
        super().__init__(ORACLE_USER, ORACLE_PASSWORD, ORACLE_URL)

    @staticmethod
    def select_tb_ot_update_fb_idtarea_ob_fecha_desc(idtarea):
        default_headers = ('IDTAREA', 'ESTADO', 'CAUSA', 'SOLUCION', 'ALARMAS', 'DETALLE', 'FECHA', 'IDESTADOTAREA')
        params = {'idtarea': idtarea}

        def query(cursor, headers=default_headers):
            cursor.prepare('''
                select TB_OT_UPDATE.IDTAREA,
                       TB_OT_UPDATE.ESTADO,
                       TB_OT_UPDATE.CAUSA,
                       TB_OT_UPDATE.SOLUCION,
                       TB_OT_UPDATE.ALARMAS,
                       TB_OT_UPDATE.DETALLE,
                       TB_OT_UPDATE.FECHA,
                       TB_ESTADOTAREA.IDESTADOTAREA

                from USWEBPROD.TB_OT_UPDATE                
                left join USWEBPROD.TB_ESTADOTAREA
                on USWEBPROD.TB_OT_UPDATE.ESTADO = TB_ESTADOTAREA.NOMBREESTADOTAREAOT

                where TB_OT_UPDATE.IDTAREA = :idtarea
                order by TB_OT_UPDATE.FECHA desc
            ''')
            cursor.execute(None, params)
            return QueryResult(result=cursor.fetchall(), headers=headers)

        return query

    @staticmethod
    def select_tb_evento_fb_idticket_and_estado_cerrado_or_cancelado(ls_idticket):
        default_headers = ('IDTICKET', 'ESTADOEVENTO')
        params = {}

        ls_idticket = ["'{}'".format(item) for item in ls_idticket]
        ls_idticket = ', '.join(ls_idticket)

        def query(cursor, headers=default_headers):
            if not ls_idticket:
                return QueryResult(result=[], headers=headers)

            cursor.prepare('''
                select IDTICKET, ESTADOEVENTO 
                from USWEBPROD.TB_EVENTO 
                where IDTICKET in ({})
                and ESTADOEVENTO in ('Cerrado', 'Cancelado')
            '''.format(ls_idticket))
            cursor.execute(None, params)
            return QueryResult(result=cursor.fetchall(), headers=headers)

        return query

    @staticmethod
    def select_tb_evento_lj_tb_usuario_fb_id_ticket(id_ticket):
        default_headers = ('IDTICKET', 'NOMBRES', 'CORREO', 'TELEFONO', 'USERNAME', 'NOMBRETIPOUSUARIO')
        params = {'id_ticket': id_ticket}

        def query(cursor, headers=default_headers):
            cursor.prepare('''
                select TB_EVENTO.IDTICKET, TB_USUARIO.NOMBRES, TB_USUARIO.CORREO, TB_USUARIO.TELEFONO, TB_USUARIO.USERNAME, TB_TIPOUSUARIO.NOMBRETIPOUSUARIO
                from USWEBPROD.TB_USUARIO 
                left join USWEBPROD.TB_EVENTO
                on TB_USUARIO.USERNAME = USWEBPROD.TB_EVENTO.ING_ASIG_USER
                left join USWEBPROD.TB_TIPOUSUARIO 
                on TB_USUARIO.TIPOUSUARIO = TB_TIPOUSUARIO.IDTIPOUSUARIO
                where TB_EVENTO.IDTICKET = :id_ticket
            ''')
            cursor.execute(None, params)
            return QueryResult(result=cursor.fetchall(), headers=headers)

        return query

    @staticmethod
    def select_tb_pop_fb_idpop(idpop):
        params = {'idpop': idpop}
        default_headers = ('idpop', 'pop')

        def query(cursor, headers=default_headers):
            cursor.prepare('select IDPOP, POP from USWEBPROD.TB_POP where IDPOP = :idpop')
            cursor.execute(None, params)
            return QueryResult(result=cursor.fetchall(), headers=headers)

        return query

    @staticmethod
    def select_tb_ne_lj_inei_fb_ls_idnodo(ls_idnodo):
        ls_idnodo = ["'{}'".format(id) for id in ls_idnodo]
        ls_idnodo = ', '.join(ls_idnodo)
        params = {}
        default_headers = (
            'nombrene', 'idubigeo', 'codigo_dep', 'nomb_dep', 'codigo_pro', 'nomb_prov', 'codigo_dis', 'nomb_dist')

        def query(cursor, headers=default_headers):
            if not ls_idnodo:
                return QueryResult(result=[], headers=headers)
            cursor.prepare('''
                select tb.nombrene,
                tb.idubigeo,
                inei.codigo_dep,
                inei.nomb_dep,
                inei.codigo_pro,
                inei.nomb_prov,
                inei.codigo_dis,
                inei.nomb_dist
                from USWEBPROD.TB_NE tb
                left join usaonoc.inei_2002_distritos_region inei on inei.CODIGO_DIS = SUBSTR(tb.IDUBIGEO, 3)
                where
                idestadone = '1'
                and   tb.estado = 'ACT'
                and   tb.idclasene in ('82','83','84','85','86','87','88','89','90','91')
                and substr(tb.nombrene,1,7) in ({})
            '''.format(ls_idnodo))
            cursor.execute(None, params)
            return QueryResult(result=cursor.fetchall(), headers=headers)

        return query

    @staticmethod
    def select_tb_ne_lj_tb_tipo_ne_lj_tb_pop_fb_idne(idne):
        '''
        Return a function that works as a query
        :param str nombre_ne:
        :param int limit:
        :param EstadoGenericoEnum estado:
        :return:
        '''
        params = {'idne': idne}
        default_headers = (
            'IDNE', 'NOMBRENE', 'POP_A', 'POP_B', 'IDPOPAUX', 'ESTADO', 'IDTIPONE', 'NOMBRETIPONE', 'NOMBREPOP_A',
            'NOMBREPOP_B')

        def query(cursor, headers=default_headers):
            cursor.prepare('''
                select 
                    TB_NE.IDNE, 
                    TB_NE.NOMBRENE, 
                    TB_NE.POP_A, 
                    TB_NE.POP_B, 
                    TB_NE.IDPOPAUX, 
                    TB_NE.ESTADO,
                    TB_TIPO_NE.IDTIPONE, 
                    TB_TIPO_NE.NOMBRE,
                    TB_POP_A.POP NOMBREPOP_A,
                    TB_POP_B.POP NOMBREPOP_B   
                from USWEBPROD.TB_NE 
                left join USWEBPROD.TB_TIPO_NE
                on TB_NE.IDTIPONE = TB_TIPO_NE.IDTIPONE
                left join USWEBPROD.TB_POP TB_POP_A
                on TB_NE.POP_A = TB_POP_A.IDPOP
                left join USWEBPROD.TB_POP TB_POP_B
                on TB_NE.POP_B = TB_POP_B.IDPOP
                where IDNE = :idne
            ''')
            cursor.execute(None, params)
            return QueryResult(result=cursor.fetchall(), headers=headers)

        return query

    @staticmethod
    def select_tb_ne_left_join_inei_2002_distritos_region_fb_idticket(id_ticket):
        params = {'id_ticket': id_ticket}
        default_headers = (
            'nombrene', 'idubigeo', 'id_afectacion', 'inicio_afectacion', 'fin_afectacion', 'codigo_dep', 'nomb_dep',
            'codigo_pro', 'nomb_prov', 'codigo_dis', 'nomb_dist'
        )

        def query(cursor, headers=default_headers):
            cursor.prepare('''
                select
                   tb.nombrene,
                   tb.idubigeo,
                   tbna.idmult,
                   tbna.inicio,
                   tbna.fin,
                   inei.codigo_dep,
                   inei.nomb_dep,
                   inei.codigo_pro,
                   inei.nomb_prov,
                   inei.codigo_dis,
                   inei.nomb_dist
                from USWEBPROD.TB_TICKET_NEA_MULT tbna
                    left join USWEBPROD.TB_NE tb on tb.IDNE = tbna.IDNE
                    left join usaonoc.inei_2002_distritos_region inei on inei.CODIGO_DIS = SUBSTR(tb.IDUBIGEO, 3)
                where tbna.IDTICKET = :id_ticket
                ''')
            cursor.execute(None, params)
            return QueryResult(result=cursor.fetchall(), headers=headers)

        return query

    @staticmethod
    def count_tb_tarea_ot_left_join_tb_estado_tarea(idticket, estadotareaot):
        """

        :param str idticket:
        :param entel_core.enums.EstadoTareaOtEnum estadotareaot:
        :return:
        """
        params = {'idticket': idticket, 'idestadotareaot': estadotareaot.value}
        default_headers = ('cantidad',)

        def query(cursor, headers=default_headers):
            cursor.prepare(
                '''
                    select count(*) from USWEBPROD.TB_TAREA_OT left join USWEBPROD.TB_ESTADOTAREA 
                    on TB_TAREA_OT.ESTADOTAREA = TB_ESTADOTAREA.IDESTADOTAREA 
                    WHERE IDESTADOTAREA = :idestadotareaot
                    and IDTICKET = :idticket         
                ''')
            cursor.execute(None, params)
            return QueryResult(result=cursor.fetchall(), headers=headers)

        return query

    @staticmethod
    def select_tb_tarea_ot_lj_tb_estado_tarea_fb_idtareaot(idtareaot):
        params = {'idtareaot': idtareaot}
        default_headers = (
            'IDTAREAOT', 'CODTAREA', 'IDNE', 'COM_OBSERVA', 'COM_CANCELA', 'COM_GENERA', 'RESUELTOTAREA',
            'IDESTADOTAREA', 'NOMBREESTADOTAREA')

        def query(cursor, headers=default_headers):
            cursor.prepare('''
            select 
                TB_TAREA_OT.IDTAREAOT, 
                TB_TAREA_OT.CODTAREA, 
                TB_TAREA_OT.IDNE, 
                TB_TAREA_OT.COM_OBSERVA,
                TB_TAREA_OT.COM_CANCELA,
                TB_TAREA_OT.COM_GENERA,
                TB_TAREA_OT.RESUELTOTAREA,                
                TB_ESTADOTAREA.IDESTADOTAREA, 
                TB_ESTADOTAREA.NOMBREESTADOTAREAOT
            from USWEBPROD.TB_TAREA_OT
            left join USWEBPROD.TB_ESTADOTAREA
            on TB_TAREA_OT.ESTADOTAREA = TB_ESTADOTAREA.IDESTADOTAREA
            where IDTAREAOT = :idtareaot
            ''')
            cursor.execute(None, params)
            return QueryResult(result=cursor.fetchall(), headers=headers)

        return query

    @staticmethod
    def select_tb_tarea_ot_lj_tb_estado_tarea_fb_idticket(idticket):
        params = {'idticket': idticket}
        default_headers = (
            'IDTAREAOT', 'CODTAREA', 'IDNE', 'COM_OBSERVA', 'COM_CANCELA', 'COM_GENERA', 'RESUELTOTAREA',
            'IDESTADOTAREA', 'NOMBREESTADOTAREA')

        def query(cursor, headers=default_headers):
            cursor.prepare('''
            select 
                TB_TAREA_OT.IDTAREAOT, 
                TB_TAREA_OT.CODTAREA, 
                TB_TAREA_OT.IDNE, 
                TB_TAREA_OT.COM_OBSERVA,
                TB_TAREA_OT.COM_CANCELA,
                TB_TAREA_OT.COM_GENERA,
                TB_TAREA_OT.RESUELTOTAREA,                
                TB_ESTADOTAREA.IDESTADOTAREA, 
                TB_ESTADOTAREA.NOMBREESTADOTAREAOT
            from USWEBPROD.TB_TAREA_OT
            left join USWEBPROD.TB_ESTADOTAREA
            on TB_TAREA_OT.ESTADOTAREA = TB_ESTADOTAREA.IDESTADOTAREA
            where TB_TAREA_OT.IDTICKET = :idticket
            ''')
            cursor.execute(None, params)
            return QueryResult(result=cursor.fetchall(), headers=headers)

        return query

    @staticmethod
    def select_tb_tarea_ot_fb_idticket_ob_fechacreacion_desc(id_ticket):
        '''

        :param str idticket:
        :return:
        '''
        params = {'id_ticket': id_ticket}
        default_headers = ('IDTAREAOT', 'IDTICKET', 'IDNE', 'CODTAREA', 'ESTADOTAREA', 'FECHACREACION', 'RESUELTOTAREA')

        def query(cursor, headers=default_headers):
            cursor.prepare('''
                select IDTAREAOT, IDTICKET, IDNE, CODTAREA, ESTADOTAREA, FECHACREACION, RESUELTOTAREA
                from USWEBPROD.TB_TAREA_OT
                where IDTICKET = :id_ticket
                order by FECHACREACION desc 
            ''')
            cursor.execute(None, params)
            return QueryResult(result=cursor.fetchall(), headers=headers)

        return query

    @staticmethod
    def select_tb_energia_alternativa_fb_idnodo(id_ticket, idnodo, rango_fecha_consulta):
        '''
        :param str idnodo:
        :return:
        '''
        params = {'id_ticket': id_ticket, 'idnodo': idnodo, 'rango_fecha_consulta': rango_fecha_consulta}
        default_headers = ('ID', 'IDEVENTO', 'NODE', 'FECHACONSULTA', 'REMAINING_CAPACITY', 'BATTERY_CURRENT',
                           'CHARGING_DISCHARGING_STATE', 'BUSBAR_VOLTAGE', 'LOAD_TOTAL_CURRENT',
                           'BATTERY_CURRENT_LIMITING_COEF', 'IDTICKET', 'IDNE', 'BATTERY_CAPACITY',
                           'BATTERY_CURRENT_PMU')

        def query(cursor, headers=default_headers):
            cursor.prepare('''
                    select ID, IDEVENTO, NODE, FECHACONSULTA, REMAINING_CAPACITY, BATTERY_CURRENT,
                        CHARGING_DISCHARGING_STATE, BUSBAR_VOLTAGE, LOAD_TOTAL_CURRENT, BATTERY_CURRENT_LIMITING_COEF,
                        IDTICKET, IDNE, BATTERY_CAPACITY, BATTERY_CURRENT_PMU
                    from (
                        select * from USWEBPROD.TB_ENERGIA_ALTERNATIVA 
                        where substr(node,1,7) = :idnodo and IDTICKET = :id_ticket
                        AND FECHACONSULTA > SYSDATE - NUMTODSINTERVAL(:rango_fecha_consulta, 'MINUTE')
                        order by FECHACONSULTA desc)
                    where rownum = 1
                ''')
            cursor.execute(None, params)
            return QueryResult(result=cursor.fetchall(), headers=headers)

        return query

    @staticmethod
    def select_tb_tipoincidente_fb_nombre(nombre_tipo_incidente, limit=1, estado=EstadoGenericoEnum.ACTIVO):
        '''
        Return a function that works as a query
        :param str nombre_tipo_incidente:
        :param int limit:
        :param EstadoGenericoEnum estado:
        :return:
        '''
        params = {'nombre_tipo_incidente': nombre_tipo_incidente, 'limit': limit, 'estado': estado.value}

        def query(cursor, headers=(
                'IDTIPOINCIDENTE', 'NOMBRETIPOINCIDENTE', 'PRIORIDAD', 'CORREO1CC', 'CORREO2CC', 'NOMBREENCARGADO',
                'ENCARGADO')):
            cursor.prepare('''
                select IDTIPOINCIDENTE, NOMBRETIPOINCIDENTE, PRIORIDAD, CORREO1CC, CORREO2CC, ENCARGADO, ENCARGADO 
                from USWEBPROD.TB_TIPOINCIDENTE 
                WHERE NOMBRETIPOINCIDENTE = :nombre_tipo_incidente and ESTADO = :estado
                fetch next :limit rows only
            ''')
            cursor.execute(None, params)
            return QueryResult(result=cursor.fetchall(), headers=headers)

        return query

    @staticmethod
    def select_tb_nombreincidente_fb_nombre(nombre, limit=1, estado=EstadoGenericoEnum.ACTIVO):
        '''
        Return a function that works as a query
        :param str nombre:
        :param int limit:
        :param EstadoGenericoEnum estado:
        :return:
        '''
        params = {'nombre': nombre, 'limit': limit, 'estado': estado.value}

        def query(cursor, headers=('NOMBRE', 'TIPOSINCIDENTE')):
            cursor.prepare('''
                select NOMBRE, TIPOSINCIDENTE from USWEBPROD.TB_NOMBREINCIDENTE 
                where ESTADO = :estado and NOMBRE = :nombre
                fetch next :limit rows only
            ''')
            cursor.execute(None, params)
            return QueryResult(result=cursor.fetchall(), headers=headers)

        return query

    @staticmethod
    def select_tb_usuario_fb_username(username, limit=1, estado=EstadoGenericoEnum.ACTIVO):
        '''
        Return a function that works as a query
        :param str username:
        :param int limit:
        :param EstadoGenericoEnum estado:
        :return:
        '''
        params = {'username': username, 'limit': limit, 'estado': estado.value}

        def query(cursor, headers=('USERNAME', 'NOMBRES', 'PUESTO', 'GERENCIA', 'CORREO', 'TIPOUSUARIO')):
            cursor.prepare('''
                select USERNAME, NOMBRES, PUESTO, GERENCIA, CORREO, TIPOUSUARIO 
                from USWEBPROD.TB_USUARIO where ESTADO = :estado and USERNAME = :username
                fetch next :limit rows only
            ''')
            cursor.execute(None, params)
            return QueryResult(result=cursor.fetchall(), headers=headers)

        return query

    @staticmethod
    def select_tb_estadoticket_fb_nombre(nombre_estado_ticket, limit=1, estado=EstadoGenericoEnum.ACTIVO):
        '''
        Return a function that works as a query
        :param str nombre_estado_ticket:
        :param int limit:
        :param EstadoGenericoEnum estado:
        :return:
        '''
        params = {'nombre_estado_ticket': nombre_estado_ticket, 'limit': limit, 'estado': estado.value}

        def query(cursor, headers=('IDESTADOTICKET', 'NOMBREESTADOTICKET', 'DEFAULT1', 'COLOR')):
            cursor.prepare('''
                select IDESTADOTICKET, NOMBREESTADOTICKET, DEFAULT1, COLOR from USWEBPROD.TB_ESTADOTICKET 
                where ESTADO = :estado and NOMBREESTADOTICKET = :nombre_estado_ticket
                fetch next :limit rows only
            ''')
            cursor.execute(None, params)
            return QueryResult(result=cursor.fetchall(), headers=headers)

        return query

    @staticmethod
    def select_tb_ne_fb_idnodo(id_nodo, limit=1, estado=EstadoGenericoEnum.ACTIVO):
        '''
        Return a function that works as a query
        :param str id_nodo:
        :param int limit:
        :param EstadoGenericoEnum estado:
        :return:
        '''
        params = {'id_nodo': id_nodo, 'limit': limit, 'estado': estado.value}

        def query(cursor, headers=('IDNE', 'NOMBRENE', 'POP_A', 'POP_B', 'IDTIPONE', 'IDPOPAUX', 'ESTADO')):
            cursor.prepare('''
                select IDNE, NOMBRENE, POP_A, POP_B, IDTIPONE, IDPOPAUX, ESTADO 
                from USWEBPROD.TB_NE 
                where ESTADO = :estado 
                and substr(NOMBRENE,1,7) = :id_nodo
                fetch next :limit rows only
            ''')
            cursor.execute(None, params)
            return QueryResult(result=cursor.fetchall(), headers=headers)

        return query

    @staticmethod
    def select_tb_ne_left_join_tb_clase_ne_fb_idnodo(
            idnodo: str,
            estado: EstadoGenericoEnum = EstadoGenericoEnum.ACTIVO,
            estado_ne: EstadoNeEnum = EstadoNeEnum.ON_AIR
    ):
        params = {'idnodo': idnodo, 'estado': estado.value, 'estado_ne': estado_ne.value}

        def query(cursor,
                  headers=('IDNE', 'NOMBREIDNE', 'NOMBRENE', 'NOMBRECLASENE', 'COMENTARIO', 'PRIORIDAD', 'FLAG700MHZ',
                           'OYMRESPONSABLE', 'POP_A', 'POP_B', 'ESTADO')):
            cursor.prepare('''
                select TB_NE.IDNE,
                       TB_NE.NOMBREIDNE,
                       TB_NE.NOMBRENE,
                       TB_CLASE_NE.NOMBRE NOMBRECLASENE,
                       TB_NE.COMENTARIO,
                       TB_NE.PRIORIDAD,
                       TB_NE.FLAG700MHZ,
                       TB_NE.OYMRESPONSABLE,
                       TB_NE.POP_A,
                       TB_NE.POP_B,
                       TB_NE.ESTADO
                from USWEBPROD.TB_NE
                       left join
                     USWEBPROD.TB_CLASE_NE on USWEBPROD.TB_NE.IDCLASENE = USWEBPROD.TB_CLASE_NE.IDCLASENE
                where TB_NE.ESTADO = :estado and substr(TB_NE.NOMBRENE,1,7) = :idnodo and TB_NE.IDESTADONE = :estado_ne
            ''')
            cursor.execute(None, params)
            return QueryResult(result=cursor.fetchall(), headers=headers)

        return query

    @staticmethod
    def select_tb_ticket_to_update_fb_idticket(id_ticket, limit=1):
        '''
        Return a function that works as a query
        :param str id_ticket:
        :param int limit:
        :return:
        '''
        params = {'id_ticket': id_ticket, 'limit': limit}

        def query(
                cursor,
                headers=(
                        'IDTICKET', 'FECHACREACION', 'FECHAASIGNADO', 'ESTADOEVENTO', 'TIPOEVENTO', 'NOMBREEVENTO',
                        'OTRONOMBRE', 'PRIORIDAD', 'INGENIEROASIGNADO', 'INGENIERONOC', 'MTTO', 'DESCARGABB',
                        'TICKETPROVEEDOR', 'FECHAINICIOEVENTO', 'FECHAFINEVENTO', 'FECHAHORAPENDIENTE',
                        'MOTIVOPENDIENTE', 'AFECTACION', 'NESPROBLEMA', 'NESAFECTACION',
                        'MOTIVOASIGNADO', 'USUARIOCREACION', 'USUARIOMODIFICACION', 'IDNOMBRE', 'FLAG700MHZ',
                        'MODOCALCPRIORIDAD', 'IDMOTIVO', 'NOMBREMOTIVO', 'IDPRIORIDADTICKET',
                        'NOMBREPRIORIDADTICKET', 'CODIGO_OT', 'COLORPRIORIDAD', 'ORDEN', 'IDESTADOTICKET',
                        'NOMBREESTADOTICKET',
                        'DEFAULT1', 'COLORESTADO', 'IDMANTENIMIENTO', 'NOMBREMANTENIMIENTO', 'CODIGO_OT',
                        'IDTIPOINCIDENTE',
                        'NOMBRETIPOINCIDENTE', 'PRIORIDAD', 'CORREO1CC', 'CORREO2CC', 'ENCARGADO', 'IDIMPACTO',
                        'NOMBREIMPACTO',
                        'IDURGENCIA', 'NOMBREURGENCIA', 'USERNAME', 'NOMBRES', 'PUESTO', 'GERENCIA', 'CORREO',
                        'TIPOUSUARIO'
                )
        ):
            cursor.prepare('''
                select TB_TICKET.IDTICKET,
                       TB_EVENTO.FECHACREACION,
                        TB_EVENTO.FECHAASIGNADO,
                       TB_EVENTO.ESTADOEVENTO,
                       TB_EVENTO.TIPOEVENTO,
                       TB_EVENTO.NOMBRE,
                       TB_EVENTO.OTRONOMBRE,
                       TB_EVENTO.PRIORIDAD,
                       TB_EVENTO.INGENIEROASIGNADO,
                       TB_EVENTO.INGENIERONOC,
                       TB_EVENTO.MTTO,
                       TB_EVENTO.DESCARGABB,
                       TB_EVENTO.TICKETPROVEEDOR,
                       TB_EVENTO.FECHAINICIOEVENTO,
                       TB_EVENTO.FECHAFINEVENTO,
                       TB_EVENTO.FECHAHORAPENDIENTE,
                       TB_EVENTO.MOTIVOPENDIENTE,
                       TB_EVENTO.AFECTACION,
                       TB_EVENTO.NESPROBLEMA,
                       TB_EVENTO.NESAFECTACION,
                       TB_EVENTO.MOTIVOASIGNADO,
                       TB_EVENTO.USUARIOCREACION,
                       TB_EVENTO.USUARIOMODIFICACION,
                       TB_EVENTO.IDNOMBRE,
                       TB_EVENTO.FLAG700MHZ,
                       TB_EVENTO.MODOCALCPRIORIDAD,
                       TB_MOTIVO_ASIGNACION.IDMOTIVO,
                       TB_MOTIVO_ASIGNACION.NOMBRE,
                       TB_PRIORIDADTICKET.IDPRIORIDADTICKET,
                       TB_PRIORIDADTICKET.NOMBREPRIORIDADTICKET,
                       TB_PRIORIDADTICKET.CODIGO_OT,
                       TB_PRIORIDADTICKET.COLOR,
                       TB_PRIORIDADTICKET.ORDEN,
                       TB_ESTADOTICKET.IDESTADOTICKET,
                       TB_ESTADOTICKET.NOMBREESTADOTICKET,
                       TB_ESTADOTICKET.DEFAULT1,
                       TB_ESTADOTICKET.COLOR,
                       TB_MANTENIMIENTO.IDMANTENIMIENTO,
                       TB_MANTENIMIENTO.NOMBREMANTENIMIENTO,
                       TB_MANTENIMIENTO.CODIGO_OT,
                       TB_TIPOINCIDENTE.IDTIPOINCIDENTE,
                       TB_TIPOINCIDENTE.NOMBRETIPOINCIDENTE,
                       TB_TIPOINCIDENTE.PRIORIDAD,
                       TB_TIPOINCIDENTE.CORREO1CC,
                       TB_TIPOINCIDENTE.CORREO2CC,
                       TB_TIPOINCIDENTE.ENCARGADO,
                       TB_IMPACTO.IDIMPACTO,
                       TB_IMPACTO.NOMBRE,
                       TB_URGENCIA.IDURGENCIA,
                       TB_URGENCIA.NOMBRE,
                       TB_USUARIO.USERNAME,
                       TB_USUARIO.NOMBRES,
                       TB_USUARIO.PUESTO,
                       TB_USUARIO.GERENCIA,
                       TB_USUARIO.CORREO,
                       TB_USUARIO.TIPOUSUARIO
                from USWEBPROD.TB_TICKET
                       left join USWEBPROD.TB_EVENTO
                                 on TB_TICKET.IDTICKET = TB_EVENTO.IDTICKET
                       left join USWEBPROD.TB_MOTIVO_ASIGNACION
                                 on TB_EVENTO.MOTIVOASIGNADO = TB_MOTIVO_ASIGNACION.IDMOTIVO
                       left join USWEBPROD.TB_PRIORIDADTICKET
                                 on TB_EVENTO.IDPRIORIDAD = TB_PRIORIDADTICKET.IDPRIORIDADTICKET
                       left join USWEBPROD.TB_ESTADOTICKET
                                 on TB_ESTADOTICKET.NOMBREESTADOTICKET = TB_EVENTO.ESTADOEVENTO
                       left join USWEBPROD.TB_MANTENIMIENTO
                                 on TB_MANTENIMIENTO.NOMBREMANTENIMIENTO = TB_EVENTO.MTTO
                       left join USWEBPROD.TB_TIPOINCIDENTE
                                 on TB_TIPOINCIDENTE.NOMBRETIPOINCIDENTE = TB_EVENTO.TIPOEVENTO
                       left join USWEBPROD.TB_IMPACTO
                                 on TB_IMPACTO.IDIMPACTO = TB_EVENTO.IDIMPACTO
                       left join USWEBPROD.TB_URGENCIA
                                 on TB_URGENCIA.IDURGENCIA = TB_EVENTO.IDURGENCIA
                       left join USWEBPROD.TB_USUARIO
                                 on TB_USUARIO.USERNAME = TB_EVENTO.ING_ASIG_USER
                where TB_TICKET.IDTICKET = :id_ticket
                fetch next :limit rows only 
            ''')
            cursor.execute(None, params)
            return QueryResult(result=cursor.fetchall(), headers=headers)

        return query

    @staticmethod
    def select_tb_ticket_ne_problema_left_join_tb_ne_fb_ls_idnodo(ls_idnodo):
        '''

        :param list ls_idnodo: Listado de nombres del nodo(NOMBRENE)
        :return:
        '''
        ls_idnodo = ["'{}'".format(id) for id in ls_idnodo]
        ls_idnodo = ', '.join(ls_idnodo)
        params = {}
        default_headers = (
            'idticket', 'idne', 'fechacreacion', 'nombrene', 'tipoevento', 'nombreevento', 'fechainicioevento',
            'fechafinevento'
        )

        def query(cursor, headers=default_headers):
            if not ls_idnodo:
                return QueryResult(result=[], headers=headers)
            if os.getenv('SIMULATION_MODE') == 'False':
                cursor.prepare('''
                    select 
                        a.idticket, 
                        a.idne, 
                        a.FECHACREACION,
                        c.nombrene, 
                        b.tipoevento,
                        b.nombre,
                        b.fechainicioevento,
                        b.FECHAFINEVENTO
                    from uswebprod.tb_ticket_ne_problema a
                           inner join (select b.idticket,
                                              b.estadoevento,
                                              b.tipoevento,
                                              b.nombre,
                                              b.idnombre,
                                              b.fechainicioevento,
                                              b.FECHAFINEVENTO,
                                              b.fechacreacion,
                                              round((b.fechacreacion - b.fechainicioevento) * 24 * 60, 4) diff_min
                                       from uswebprod.tb_evento b) b
                                      on (a.idticket = b.idticket)
                           left join (select c.idne, c.nombrene, c.prioridad from uswebprod.tb_ne c) c
                                     on (a.idne = c.idne)
                    where b.estadoevento <> ('Cancelado')
                      and b.estadoevento <> ('Cerrado')
                      and b.tipoevento = 'Energia'
                      and b.nombre in ('Corte de Energia Comercial', 'Sobre voltaje de AC comercial', 'Bajo voltaje de AC comercial')
                      and substr(c.nombrene,1,7) in ({})
                    order by a.idticket desc
                '''.format(ls_idnodo))
            else:
                cursor.prepare('''
                    select 
                        a.idticket, 
                        a.idne, 
                        a.FECHACREACION,
                        c.nombrene, 
                        b.tipoevento,
                        b.nombre,
                        b.fechainicioevento,
                        b.FECHAFINEVENTO
                    from uswebprod.tb_ticket_ne_problema a
                           inner join (select b.idticket,
                                              b.estadoevento,
                                              b.tipoevento,
                                              b.nombre,
                                              b.idnombre,
                                              b.fechainicioevento,
                                              b.FECHAFINEVENTO,
                                              b.fechacreacion,
                                              round((b.fechacreacion - b.fechainicioevento) * 24 * 60, 4) diff_min
                                       from uswebprod.tb_evento b) b
                                      on (a.idticket = b.idticket)
                           left join (select c.idne, c.nombrene, c.prioridad from uswebprod.tb_ne c) c
                                     on (a.idne = c.idne)
                    where b.estadoevento <> ('Cancelado')
                      and b.estadoevento <> ('Cerrado')
                      and b.tipoevento = 'Energia'
                      and b.nombre = 'PRUEBAS DE AUTOMATIZACIÓN (No Considerar)'
                      and substr(c.nombrene,1,7) in ({})
                    order by a.idticket desc
                '''.format(ls_idnodo))

            cursor.execute(None, params)
            return QueryResult(result=cursor.fetchall(), headers=headers)

        return query

    @staticmethod
    def select_tb_ticket_ne_problema_lj_tb_ne_fb_idticket(idticket):
        params = {'idticket': idticket}
        default_headers = (
            'idticket', 'idne', 'fechacreacion', 'nombrene', 'tipoevento', 'nombreevento', 'fechainicioevento',
            'fechafinevento'
        )

        def query(cursor, headers=default_headers):
            if os.getenv('SIMULATION_MODE') == 'False':
                cursor.prepare('''
                    select 
                        a.idticket, 
                        a.idne, 
                        a.FECHACREACION,
                        c.nombrene, 
                        b.tipoevento,
                        b.nombre,
                        b.fechainicioevento,
                        b.FECHAFINEVENTO
                    from uswebprod.tb_ticket_ne_problema a
                           inner join (select b.idticket,
                                              b.estadoevento,
                                              b.tipoevento,
                                              b.nombre,
                                              b.idnombre,
                                              b.fechainicioevento,
                                              b.FECHAFINEVENTO,
                                              b.fechacreacion,
                                              round((b.fechacreacion - b.fechainicioevento) * 24 * 60, 4) diff_min
                                       from uswebprod.tb_evento b) b
                                      on (a.idticket = b.idticket)
                           left join (select c.idne, c.nombrene, c.prioridad from uswebprod.tb_ne c) c
                                     on (a.idne = c.idne)
                    where a.IDTICKET = :idticket
                ''')
            else:
                cursor.prepare('''
                    select 
                        a.idticket, 
                        a.idne, 
                        a.FECHACREACION,
                        c.nombrene, 
                        b.tipoevento,
                        b.nombre,
                        b.fechainicioevento,
                        b.FECHAFINEVENTO
                    from uswebprod.tb_ticket_ne_problema a
                           inner join (select b.idticket,
                                              b.estadoevento,
                                              b.tipoevento,
                                              b.nombre,
                                              b.idnombre,
                                              b.fechainicioevento,
                                              b.FECHAFINEVENTO,
                                              b.fechacreacion,
                                              round((b.fechacreacion - b.fechainicioevento) * 24 * 60, 4) diff_min
                                       from uswebprod.tb_evento b) b
                                      on (a.idticket = b.idticket)
                           left join (select c.idne, c.nombrene, c.prioridad from uswebprod.tb_ne c) c
                                     on (a.idne = c.idne)
                    where a.IDTICKET = :idticket
                ''')

            cursor.execute(None, params)
            return QueryResult(result=cursor.fetchall(), headers=headers)

        return query

    @staticmethod
    def select_tb_evento_fb_notin_estadoevento_and_cat_ope1_and_cat_ope_2(ls_notin_estadoevento, cat_ope_1, cat_ope_2,
                                                                          idnodo):
        """

        :param list of entel_core.enums.EstadoTicketEnum ls_notin_estadoevento:
        :param str cat_ope_1:
        :param str cat_ope_2:
        :return:
        """
        params = {'cat_ope_1': cat_ope_1, 'cat_ope_2': cat_ope_2, 'idnodo': idnodo}
        ls_notin_estadoevento = ["'{}'".format(item.value) for item in ls_notin_estadoevento]
        ls_notin_estadoevento = ', '.join(ls_notin_estadoevento)
        default_headers = ('IDTICKET', 'ESTADOEVENTO')

        def query(cursor, headers=default_headers):
            cursor.prepare('''
                select IDTICKET, ESTADOEVENTO from USWEBPROD.TB_EVENTO where ESTADOEVENTO 
                not in ({}) 
                and TIPOEVENTO = :cat_ope_1 
                and NOMBRE = :cat_ope_2
                and substr(nesproblema,1,7) = :idnodo
            '''.format(ls_notin_estadoevento))
            cursor.execute(None, params)
            return QueryResult(result=cursor.fetchall(), headers=headers)

        return query

    @staticmethod
    def select_tb_prioridadticket_fb_idprioridadticket(prioridadticket: PrioridadTicketEnum):
        params = {'idprioridadticket': prioridadticket.value}
        default_headers = ('IDPRIORIDAD', 'NOMBREPRIORIDADTICKET', 'CODIGO_OT', 'COLOR', 'ORDEN')

        def query(cursor, headers=default_headers):
            cursor.prepare('''
                select IDPRIORIDADTICKET, NOMBREPRIORIDADTICKET, CODIGO_OT, COLOR, ORDEN
                from USWEBPROD.TB_PRIORIDADTICKET
                where ESTADO = 'ACT'
                and IDPRIORIDADTICKET = :idprioridadticket
            ''')
            cursor.execute(None, params)
            return QueryResult(result=cursor.fetchall(), headers=headers)

        return query

    @staticmethod
    def select_tb_evento_fb_idticket(id_ticket):
        params = {'id_ticket': id_ticket}
        default_headers = (
            'IDTICKET', 'IDNOMBRE', 'NOMBRE', 'TIPOEVENTO', 'ESTADOEVENTO', 'FECHAINICIOEVENTO', 'FECHAFINEVENTO',
            'INGENIEROASIGNADO', 'INGENIERONOC', 'PRIORIDAD', 'NESPROBLEMA', 'FECHACREACION', 'MTTO')

        def query(cursor, headers=default_headers):
            cursor.prepare('''
                select TB_EVENTO.IDTICKET,
                       TB_EVENTO.IDNOMBRE,
                       TB_EVENTO.NOMBRE,
                       TB_EVENTO.TIPOEVENTO,
                       TB_EVENTO.ESTADOEVENTO,
                       TB_EVENTO.FECHAINICIOEVENTO,
                       TB_EVENTO.FECHAFINEVENTO,
                       TB_EVENTO.INGENIEROASIGNADO,
                       TB_EVENTO.INGENIERONOC,
                       TB_PRIORIDADTICKET.NOMBREPRIORIDADTICKET as PRIORIDAD,
                       TB_EVENTO.NESPROBLEMA,
                       TB_EVENTO.FECHACREACION,
                       TB_EVENTO.MTTO
                from USWEBPROD.TB_EVENTO
                       left join USWEBPROD.TB_PRIORIDADTICKET
                                 on TB_EVENTO.IDPRIORIDAD = TB_PRIORIDADTICKET.IDPRIORIDADTICKET
                where IDTICKET = :id_ticket
            ''')
            cursor.execute(None, params)
            return QueryResult(result=cursor.fetchall(), headers=headers)

        return query

    @staticmethod
    def select_tb_evento_fb_ls_idticket(ls_idticket):
        params = {}
        ls_idticket = ["'{}'".format(id) for id in ls_idticket]
        ls_idticket = ', '.join(ls_idticket)
        default_headers = (
            'IDTICKET', 'IDNOMBRE', 'NOMBRE', 'TIPOEVENTO', 'ESTADOEVENTO', 'FECHAINICIOEVENTO', 'FECHAFINEVENTO',
            'INGENIEROASIGNADO', 'ING_ASIG_USER', 'INGENIERONOC', 'PRIORIDAD', 'NESPROBLEMA', 'FECHACREACION', 'MTTO')

        def query(cursor, headers=default_headers):
            if not ls_idticket:
                return QueryResult(result=[], headers=headers)

            cursor.prepare('''
                select TB_EVENTO.IDTICKET,
                       TB_EVENTO.IDNOMBRE,
                       TB_EVENTO.NOMBRE,
                       TB_EVENTO.TIPOEVENTO,
                       TB_EVENTO.ESTADOEVENTO,
                       TB_EVENTO.FECHAINICIOEVENTO,
                       TB_EVENTO.FECHAFINEVENTO,
                       TB_EVENTO.INGENIEROASIGNADO,
                       TB_EVENTO.ING_ASIG_USER,
                       TB_EVENTO.INGENIERONOC,
                       TB_PRIORIDADTICKET.NOMBREPRIORIDADTICKET as PRIORIDAD,
                       TB_EVENTO.NESPROBLEMA,
                       TB_EVENTO.FECHACREACION,
                       TB_EVENTO.MTTO
                from USWEBPROD.TB_EVENTO
                       left join USWEBPROD.TB_PRIORIDADTICKET
                                 on TB_EVENTO.IDPRIORIDAD = TB_PRIORIDADTICKET.IDPRIORIDADTICKET
                where IDTICKET in ({})
            '''.format(ls_idticket))
            cursor.execute(None, params)
            return QueryResult(result=cursor.fetchall(), headers=headers)

        return query

    @staticmethod
    def select_tb_motivo_asignacion_fb_nombre(nombre, limit=1, estado=EstadoGenericoEnum.ACTIVO):
        '''
        Return a function that works as a query
        :param str nombre:
        :param int limit:
        :param EstadoGenericoEnum estado:
        :return:
        '''
        params = {'nombre': nombre, 'limit': limit, 'estado': estado.value}

        def query(cursor, headers=(
                'IDMOTIVO', 'NOMBRE', 'USUARIOCREACION', 'FECHACREACION', 'USUARIOMODIFICACION', 'FECHAMODIFICACION')):
            cursor.prepare('''
                select IDMOTIVO, NOMBRE, USUARIOCREACION, FECHACREACION, USUARIOMODIFICACION, FECHAMODIFICACION
                from USWEBPROD.TB_MOTIVO_ASIGNACION
                where TB_MOTIVO_ASIGNACION.ESTADO = :estado and TB_MOTIVO_ASIGNACION.NOMBRE = :nombre
                fetch next :limit rows only
            ''')
            cursor.execute(None, params)
            return QueryResult(result=cursor.fetchall(), headers=headers)

        return query

    @staticmethod
    def select_tb_mantenimiento_fb_nombremantenimiento(nombre, limit=1):
        params = {'nombre': nombre, 'limit': limit}

        def query(cursor, headers=('IDMANTENIMIENTO', 'NOMBREMANTENIMIENTO', 'CODIGO_OT')):
            cursor.prepare('''
                select IDMANTENIMIENTO, NOMBREMANTENIMIENTO, CODIGO_OT
                from USWEBPROD.TB_MANTENIMIENTO
                where TB_MANTENIMIENTO.NOMBREMANTENIMIENTO = :nombre
                fetch next :limit rows only
            ''')
            cursor.execute(None, params)
            return QueryResult(result=cursor.fetchall(), headers=headers)

        return query

    @staticmethod
    def select_tb_ticket_ot_comentario_fb_idticket(id_ticket):
        '''
        Return a function that works as a query
        :param str id_ticket:
        :return:
        '''
        params = {'id_ticket': id_ticket}

        def query(cursor, headers=('IDTICKET', 'COMENTARIO')):
            cursor.prepare('''
                SELECT USWEBPROD.TB_TICKET_BITACORA.IDTICKET,
                USWEBPROD.TB_TICKET_BITACORA.COMENTARIO
                FROM USWEBPROD.TB_TICKET_BITACORA
                WHERE USWEBPROD.TB_TICKET_BITACORA.COMENTARIO like '%Incidente solucionado%'
                AND USWEBPROD.TB_TICKET_BITACORA.IDTICKET = :id_ticket     
                ''')
            cursor.execute(None, params)
            return QueryResult(result=cursor.fetchall(), headers=headers)

        return query

    @staticmethod
    def select_tb_energia_alternativa_fb_idticket_and_idnodo(idticket, idnodo):
        '''

        :param idticket:
        :param idnodo:
        :return:
        '''
        params = {'idticket': idticket, 'idnodo': idnodo}

        def query(cursor, headers=('ID', 'IDEVENTO', 'NODE', 'FECHACONSULTA', 'REMAINING_CAPACITY', 'BATTERY_CURRENT',
                                   'CHARGING_DISCHARGING_STATE', 'BUSBAR_VOLTAGE', 'LOAD_TOTAL_CURRENT',
                                   'BATTERY_CURRENT_LIMITING_COEF', 'IDTICKET', 'IDNE', 'BATTERY_CAPACITY',
                                   'BATTERY_CURRENT_PMU')):
            cursor.prepare('''
                select ID,
                       IDEVENTO,
                       NODE,
                       FECHACONSULTA,
                       REMAINING_CAPACITY,
                       BATTERY_CURRENT,
                       CHARGING_DISCHARGING_STATE,
                       BUSBAR_VOLTAGE,
                       LOAD_TOTAL_CURRENT,
                       BATTERY_CURRENT_LIMITING_COEF,
                       IDTICKET,
                       IDNE,
                       BATTERY_CAPACITY,
                       BATTERY_CURRENT_PMU
                from USWEBPROD.TB_ENERGIA_ALTERNATIVA
                where IDTICKET = :idticket
                  and substr(NODE,1,7) = :idnodo
                order by FECHACONSULTA asc            
            ''')  # OJO si dejamos de filtrar por ticket habria que restringir cuantas filas consulta
            cursor.execute(None, params)
            return QueryResult(result=cursor.fetchall(), headers=headers)

        return query

    @staticmethod
    def select_tb_tipoafectacionticket():
        params = {}
        default_headers = ('IDTIPOAFECTACION', 'NOMBRE')

        def query(cursor, headers=default_headers):
            cursor.prepare('''
                SELECT IDTIPOAFECTACION, NOMBRE FROM USWEBPROD.TB_TIPOAFECTACIONTICKET WHERE ESTADO = 'ACT'
            ''')
            cursor.execute(None, params)
            return QueryResult(result=cursor.fetchall(), headers=headers)

        return query

    @staticmethod
    def select_tb_tecnologiaafectacionticket():
        params = {}
        default_headers = ('IDTECNOLOGIA', 'NOMBRE')

        def query(cursor, headers=default_headers):
            cursor.prepare('''
                    SELECT IDTECNOLOGIA, NOMBRE FROM USWEBPROD.TB_TECNOLOGIAAFECTACIONTICKET WHERE ESTADO = 'ACT'
                ''')
            cursor.execute(None, params)
            return QueryResult(result=cursor.fetchall(), headers=headers)

        return query

    @staticmethod
    def select_tb_sectorafectacionticket():
        params = {}
        default_headers = ('IDSECTOR', 'NOMBRE')

        def query(cursor, headers=default_headers):
            cursor.prepare('''
                    select IDSECTOR, NOMBRE from USWEBPROD.TB_SECTORAFECTACIONTICKET where ESTADO = 'ACT'
                ''')
            cursor.execute(None, params)
            return QueryResult(result=cursor.fetchall(), headers=headers)

        return query

    @staticmethod
    def select_tb_ticket_ne_lj_tb_evento_lj_tb_ne_fb_idticket(id_ticket):
        params = {'id_ticket': id_ticket}
        default_headers = (
            'idnombre', 'nombre', 'tipoevento', 'descargabb', 'estadoevento', 'fechainicioevento', 'fechafinevento',
            'ingenieronoc', 'ingenieroasignado', 'nesproblema', 'mtto', 'nombrene', 'prioridad')

        def query(cursor, headers=default_headers):
            cursor.prepare('''
                select TB_EVENTO.IDNOMBRE,
                       TB_EVENTO.NOMBRE,
                       TB_EVENTO.TIPOEVENTO,
                       TB_EVENTO.DESCARGABB,
                       TB_EVENTO.ESTADOEVENTO,
                       TB_EVENTO.FECHAINICIOEVENTO,
                       TB_EVENTO.FECHAFINEVENTO,
                       TB_EVENTO.INGENIERONOC,
                       TB_EVENTO.INGENIEROASIGNADO,
                       TB_EVENTO.NESPROBLEMA,
                       TB_EVENTO.MTTO,
                       TB_NE.NOMBRENE,
                       TB_PRIORIDADTICKET.NOMBREPRIORIDADTICKET as PRIORIDAD
                from USWEBPROD.TB_TICKET_NE_PROBLEMA
                       left join USWEBPROD.TB_EVENTO on TB_TICKET_NE_PROBLEMA.IDTICKET = TB_EVENTO.IDTICKET
                       left join USWEBPROD.TB_NE on TB_TICKET_NE_PROBLEMA.IDNE = TB_NE.IDNE
                       left join USWEBPROD.TB_PRIORIDADTICKET on TB_EVENTO.IDPRIORIDAD = TB_PRIORIDADTICKET.IDPRIORIDADTICKET
                where TB_EVENTO.IDTICKET = :id_ticket
                ''')
            cursor.execute(None, params)
            return QueryResult(result=cursor.fetchall(), headers=headers)

        return query

    @staticmethod
    def select_tb_ticket_nea_mult_fb_idticket(id_ticket):
        params = {'id_ticket': id_ticket}
        default_headers = (
            'IDMULT', 'IDTICKET', 'IDNE', 'IDAFECTACION', 'INICIO', 'FIN', 'TECNOLOGIA2G', 'TECNOLOGIA3G',
            'TECNOLOGIA4G', 'TECNOLOGIA4G700MHZ', 'CELDA', 'COMENTARIO', 'TECNOLOGIAS', 'SECTORES'
        )

        def query(cursor, headers=default_headers):
            cursor.prepare('''
                select IDMULT,
                       IDTICKET,
                       IDNE,
                       IDAFECTACION,
                       INICIO,
                       FIN,
                       TECNOLOGIA2G,
                       TECNOLOGIA3G,
                       TECNOLOGIA4G,
                       TECNOLOGIA4G700MHZ,
                       CELDA,
                       COMENTARIO,
                       TECNOLOGIAS,
                       SECTORES
                from USWEBPROD.TB_TICKET_NEA_MULT
                where IDTICKET = :id_ticket
                order by INICIO desc
            ''')
            cursor.execute(None, params)
            return QueryResult(result=cursor.fetchall(), headers=headers)

        return query

    @staticmethod
    def select_tb_ticket_nea_mult_fb_idticket_and_id_mult(id_ticket, id_mult):
        params = {'id_ticket': id_ticket, 'id_mult': id_mult}
        default_headers = (
            'IDMULT', 'IDTICKET', 'IDNE', 'IDAFECTACION', 'INICIO', 'FIN', 'TECNOLOGIA2G', 'TECNOLOGIA3G',
            'TECNOLOGIA4G', 'TECNOLOGIA4G700MHZ', 'CELDA', 'COMENTARIO', 'TECNOLOGIAS', 'SECTORES'
        )

        def query(cursor, headers=default_headers):
            cursor.prepare('''
                select IDMULT,
                       IDTICKET,
                       IDNE,
                       IDAFECTACION,
                       INICIO,
                       FIN,
                       TECNOLOGIA2G,
                       TECNOLOGIA3G,
                       TECNOLOGIA4G,
                       TECNOLOGIA4G700MHZ,
                       CELDA,
                       COMENTARIO,
                       TECNOLOGIAS,
                       SECTORES
                from USWEBPROD.TB_TICKET_NEA_MULT
                where IDMULT = :id_mult
                and IDTICKET = :id_ticket
                order by INICIO desc
            ''')
            cursor.execute(None, params)
            return QueryResult(result=cursor.fetchall(), headers=headers)

        return query

    @staticmethod
    def select_tb_ne_lj_tb_pop_lj_tb_usuario_lj_tb_contacto_fb_idnodo(idnodo):
        params = {'idnodo': idnodo}
        default_headers = (
            'IDNE', 'NOMBRENE', 'POP_A', 'OYMRESPONSABLE', 'NOMBRES', 'CORREO1', 'TELEFONO1', 'IDCLASENE'
        )

        def query(cursor, headers=default_headers):
            query = '''
                select a.IDNE,
                       a.NOMBRENE,
                       a.POP_A,
                       b.OYMRESPONSABLE,
                       c.NOMBRES,
                       d.CORREO1,
                       d.TELEFONO1,
                       a.IDCLASENE
                from uswebprod.tb_ne a

                       left join (select b.idpop, b.oymresponsable from uswebprod.tb_pop b) b
                                 on (a.pop_a = b.idpop)

                       left join (select c.username, c.nombres from uswebprod.tb_usuario c) c
                                 on (b.oymresponsable = c.username)

                       left join (select d.nombrecontacto, d.correo1, d.telefono1 from uswebprod.tb_contacto d) d
                                 on (c.nombres = d.nombrecontacto)

                where a.idclasene in ('82', '83', '84', '85', '86', '87', '88', '89', '90', '91')

                  and a.idestadone = 1
                and substr(a.NOMBRENE,1,7) = :idnodo
                '''
            cursor.execute(query, params)
            return QueryResult(result=cursor.fetchall(), headers=headers)

        return query

    @staticmethod
    def select_mops(node_id, clear_time: datetime.datetime):
        params = {
            'node_id': node_id,
            'clear_time': clear_time,
        }
        default_headers = [
            'node_id',
        ]

        def query(cursor, headers=default_headers):
            cursor.prepare('''
                select substr(tb_ne.nombrene, 1, 7) as node_id
                from tb_actividad_fecha
                         left join tb_ticket_ne on tb_actividad_fecha.idticket = tb_ticket_ne.idticket
                         left join tb_ne on tb_ne.idne = tb_ticket_ne.idne
                where substr(tb_ne.nombrene, 1, 7) = :node_id
                  and tb_actividad_fecha.inicio >= :clear_time
                  and tb_actividad_fecha.final <= :clear_time   
                ''')
            cursor.execute(None, params)
            return QueryResult(result=cursor.fetchall(), headers=headers)

        return query


def ticketero_oracle_decorator(nested_function):
    def wrapper(*args, **kwargs):
        if 'ora_ticket_con' not in kwargs:
            kwargs['ora_ticket_con'] = OracleTicketConnector()
        error, result = None, None
        try:
            result = nested_function(*args, **kwargs)
        except Exception as e:
            error = e
        finally:
            del kwargs['ora_ticket_con']
        if error:
            raise error
        return result

    return wrapper


def oym_mysql_decorator(nested_function):
    def wrapper(*args, **kwargs):
        mysql_oym_con = MySqlOymConnector()
        error, result = None, None
        try:
            result = nested_function(mysql_oym_con=mysql_oym_con, *args, **kwargs)
        except Exception as e:
            error = e
        finally:
            del mysql_oym_con
        if error:
            raise error
        return result

    return wrapper
