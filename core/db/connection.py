import os
from abc import ABC, abstractmethod

import cx_Oracle
import pandas as pd
from cx_Oracle import DatabaseError

MYSQL_USER = os.getenv('MYSQL_USER', '')
MYSQL_PASSWORD = os.getenv('MYSQL_PASSWORD', '')
MYSQL_HOST = os.getenv('MYSQL_HOST', '')
MYSQL_PORT = os.getenv('MYSQL_PORT', '')
MYSQL_NAME = os.getenv('MYSQL_NAME', '')


class QueryResult:

    def __init__(self, result, headers):
        self.result = result
        self.headers = headers
        self.result_as_dict = []
        self.result_as_df = None

    def to_dict(self):
        for row_index, row in enumerate(self.result):
            dic = {}
            for i, val in enumerate(row):
                dic[self.headers[i]] = val
            self.result_as_dict.append(dic)
        return self.result_as_dict

    def to_dataframe(self):
        '''

        :return: pandas.core.frame.DataFrame
        '''
        return pd.DataFrame.from_records(self.result, columns=self.headers)


class DbConnector(ABC):
    _connection = None

    def __del__(self):
        self.disconnect()

    @abstractmethod
    def _connect(self):
        return NotImplemented

    @abstractmethod
    def disconnect(self):
        return NotImplemented

    @abstractmethod
    def exec(self, query, headers=None):
        return NotImplemented


import mysql.connector as MysqlConector
from mysql.connector import errorcode


class MySqlConnector(DbConnector, ABC):
    _connection = None

    @abstractmethod
    def __init__(self):
        self._connect()

    def _connect(self):
        array_conection = {'user': MYSQL_USER, 'password': MYSQL_PASSWORD, 'host': MYSQL_HOST, 'port': MYSQL_PORT,
                           'database': MYSQL_NAME}
        try:
            self._connection = MysqlConector.connect(**array_conection)

        except MysqlConector.Error as err:
            if err.errno == errorcode.ER_ACCESS_DENIED_ERROR:
                print("Something is wrong with your user name or password")
            elif err.errno == errorcode.ER_BAD_DB_ERROR:
                print("Database does not exist")
            else:
                print(err)

    def disconnect(self):
        if self._connection:
            self._connection.close()

    def exec(self, query, headers=None):
        cursor = self._connection.cursor()
        try:
            result = query(cursor, headers) if headers else query(cursor)
            return result
        except Exception as e:
            # simple_logger.error(str(e))
            raise e
        finally:
            del query  # delete callback
            cursor.close()  # close cursor
            del cursor


class OracleConnector(DbConnector, ABC):
    _connection = None

    @abstractmethod
    def __init__(self, user, pwd, url):
        self.user = user
        self.pwd = pwd
        self.url = url
        self._connect()

    def _connect(self):
        try:
            self._connection = cx_Oracle.connect(self.user, self.pwd, self.url, encoding='UTF-8')
        except DatabaseError as e:
            # simple_logger.error('DatabaseError: ' + str(e))
            raise e
        except Exception as e:
            # simple_logger.error('OracleConnectorError: Error al tratar de conectar, Message: ' + str(e))
            raise e

    def disconnect(self):
        if self._connection:
            self._connection.close()

    def exec(self, query, headers=None):
        cursor = self._connection.cursor()
        try:
            result = query(cursor, headers) if headers else query(cursor)
            return result
        except Exception as e:
            print('Error executing query: ' + str(e))
        finally:
            del query  # delete callback
            cursor.close()  # close cursor
            del cursor
