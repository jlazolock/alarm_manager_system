from __future__ import absolute_import, unicode_literals

import logging
import os

# This will make sure the app is always imported when
# Django starts so that shared_task will use this app.
from django.conf import settings

from .celery import app as celery_app

# Settings display info

logger = logging.getLogger('django')
logger.warning('\n================ENVS==============')
logger.warning(f'ENV_PATH={settings.ENV_PATH}')
logger.warning(f'DJANGO_SETTINGS_MODULE={os.getenv("DJANGO_SETTINGS_MODULE")}')
logger.warning(f'CELERY_BROKER_FAKE={settings.CELERY_BROKER_FAKE}')
logger.warning('==================================\n')

# Celery config

__all__ = ('celery_app',)
