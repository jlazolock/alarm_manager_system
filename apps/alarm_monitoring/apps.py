import logging

from django.apps import AppConfig

logger = logging.getLogger('django')


class ConsumerAppConfig(AppConfig):
    name = 'apps.alarm_monitoring'

    def ready(self):
        from django_configurations.models import Configurations

        try:

            Configurations.objects.get_or_create(key='sftp.vswr_rtwp.hostname', defaults={
                'is_secret': False,
                'value': '127.0.0.1'
            })

            Configurations.objects.get_or_create(key='sftp.vswr_rtwp.port', defaults={
                'is_secret': False,
                'value': '22'
            })

            Configurations.objects.get_or_create(key='sftp.vswr_rtwp.username', defaults={
                'is_secret': True,
                'value': 'username'
            })

            Configurations.objects.get_or_create(key='sftp.vswr_rtwp.password', defaults={
                'is_secret': True,
                'value': 'password'
            })

        except Exception as e:
            logging.error(f'Error loading default config on {self.name}')
