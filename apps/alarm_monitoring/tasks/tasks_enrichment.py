import pandas as pd
import numpy as np

from alarm_manager_system.celery import app
from apps.alarm_monitoring.models import FtpAlarm
from core.db.repository import oym_mysql_decorator, MySqlOymConnector, ticketero_oracle_decorator, OracleTicketConnector


def _set_data():
    pass


def enrich_data_is_on_air(row):
    if pd.notna(row['tiempo_max_permanencia']):
        if pd.notna(row['ClearTime']):
            return row['ClearTime'] - row['FirstOccurrence']
        else:
            now = timezone.now()
            if now < row['FirstOccurrence']:
                result = datetime.timedelta()
                return result
            return now - row['FirstOccurrence']
    return None


@oym_mysql_decorator
def enrich_data_node_status(df_main: pd.DataFrame, mysql_oym_con):
    ls_node_id = df_main['node_id'].to_list()
    df_node_status = mysql_oym_con.exec(
        query=MySqlOymConnector.select_tb_nodos_gul_oa_fb_ls_idnodo(ls_node_id)
    ).to_dataframe()

    df_main = df_main.merge(
        df_node_status,
        how='left',
        left_on=('node_id',),
        right_on=('codigo',)
    )

    return df_main


@ticketero_oracle_decorator
def enrich_data_active_mops(df_main: pd.DataFrame, ora_ticket_con):
    ls_node_with_mops = []
    for idx, row in df_main.iterrows():
        dict_mops = ora_ticket_con.exec(
            OracleTicketConnector.select_mops(
                node_id=row['node_id'],
                clear_time=row['clear_datetime']
            )
        ).to_dict()
        if dict_mops:
            ls_node_with_mops.append(dict_mops['node_id'])

    df_main['active_mops'] = np.where(df_main['node_id'].isin(ls_node_with_mops), True, False)
    return df_main


def enrich_data_node_id(df_main: pd.DataFrame) -> pd.DataFrame:
    def parse_id_nodo(value):
        ls_value = value.split('_')
        if len(ls_value) < 2:
            # raise AttributeError('Error parsing id nodo of "{}"'.format(value))
            return None
        return ls_value[0]

    df_main = df_main.assign(node_id=df_main['node_name'].map(parse_id_nodo))

    return df_main


@app.task(bind=True)
def task_enrichment_ftp_vswr_rtwp(self):
    messages = ['100000']

    cols = ['id', 'serial', 'node_name', 'alarm_id', 'open_datetime', 'clear_datetime']
    ftp_alarm = FtpAlarm.objects.filter(serial__in=messages).values_list(*cols)

    df_main = pd.DataFrame(ftp_alarm, columns=cols)

    df_main = enrich_data_node_id(df_main=df_main)

    df_main = enrich_data_node_status(df_main=df_main)

    df_main = enrich_data_active_mops(df_main=df_main)

    print(df_main)
