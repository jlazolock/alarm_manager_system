import csv
import os
import re

import pysftp
from django_configurations.models import Configurations

from alarm_manager_system.celery import app
from amqp_utils import send_message, read_messages
from apps.alarm_monitoring.models import ApplicationQueuesEnum, FtpAlarm


def get_files():
    cnopts = pysftp.CnOpts()
    cnopts.hostkeys = None

    with pysftp.Connection(
            host=Configurations.objects.get(key='sftp.vswr_rtwp.hostname').clean_value,
            port=int(Configurations.objects.get(key='sftp.vswr_rtwp.port').clean_value),
            username=Configurations.objects.get(key='sftp.vswr_rtwp.username').clean_value,
            password=Configurations.objects.get(key='sftp.vswr_rtwp.password').clean_value,
            cnopts=cnopts
    ) as sftp:

        local_path_to_download = './ftp_vswr_rtwp/'
        remote_path_pending_files = '/home/nocuser/'
        remote_path_worked_files = os.path.join(remote_path_pending_files, 'csv_trabajados/')

        sftp.cwd(remote_path_pending_files)
        for filename in sftp.listdir():
            make_match = re.search("THOR_.*\.csv$", filename)
            if make_match:
                sftp.get(
                    os.path.join(remote_path_pending_files, filename),
                    os.path.join(local_path_to_download, filename)
                )
                sftp.rename(
                    os.path.join(remote_path_pending_files, filename),
                    os.path.join(remote_path_worked_files, filename)
                )
                yield os.path.join(local_path_to_download, filename)


@app.task(bind=True)
def task_consume_ftp_vswr_rtwp(self):
    for file in get_files():
        # todo aca va el codigo que leee el csv
        pass

    f = open('apps/alarm_monitoring/tasks/fixtures/data.csv', 'r')
    with f:
        reader = csv.DictReader(f)
        for row in reader:
            serial = row['identificador']
            node_name = row['ne']
            alarm_id = row['alarma']
            open_datetime = row['hora_de_inicio']
            clear_datetime = row['fecha_alarma_clareada']

            if clear_datetime == '0000-00-00 00:00:00':
                abc_alarm = FtpAlarm.objects.create(
                    serial=serial,
                    node_name=node_name,
                    alarm_id='123456',
                    open_datetime=open_datetime,
                    clear_datetime=None
                )
            else:
                abc_alarm = FtpAlarm.objects.create(
                    serial=serial,
                    node_name=node_name,
                    alarm_id='123456',
                    open_datetime=open_datetime,
                    clear_datetime=clear_datetime
                )

        print('*** *** ***')
        print('proceso terminado')
        print('*** *** ***')

    print(self)

    send_message(
        queue_name=ApplicationQueuesEnum.QUEUE_FTP_ENRICHMENT,
        message={
            'serial': '1005',
            'alarm_id': '25622',
        }
    )

    for message in read_messages(queue_name=ApplicationQueuesEnum.QUEUE_FTP_ENRICHMENT):
        print(message)
