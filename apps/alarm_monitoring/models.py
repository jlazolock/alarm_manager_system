from enum import Enum

from django.db import models


class ApplicationQueuesEnum(Enum):
    QUEUE_FTP_ENRICHMENT = 'QUEUE_FTP_ENRICHMENT'



class AbcAlarm(models.Model):
    class Meta:
        abstract = True

    serial = models.CharField(max_length=16, blank=False, null=False, unique=True)
    node_name = models.CharField(max_length=256, blank=False, null=False)
    alarm_id = models.CharField(max_length=16, blank=False, null=False)
    open_datetime = models.DateTimeField(blank=True, null=True)
    clear_datetime = models.DateTimeField(blank=True, null=True)


class FtpAlarm(AbcAlarm):
    class Meta:
        db_table = 'tb_ftp_alarm'

    # Fields to apply enrichment
    is_on_air = models.NullBooleanField()
    has_active_mops = models.NullBooleanField()
    has_active_previous_ticket = models.NullBooleanField()

    #
    initial_diagnosis = models.TextField(blank=True, null=True)
    solution = models.TextField(blank=True, null=True)


class WnmsAlarm(AbcAlarm):
    class Meta:
        db_table = 'tb_wnms_alarm'

    class WnmsEventStatus(Enum):
        OPEN = '1'
        CLOSED = '2'


class NetecoAlarm(AbcAlarm):
    class Meta:
        db_table = 'tb_neteco_alarm'
