from django.apps import AppConfig


class TicketMonitoringAppConfig(AppConfig):
    name = 'apps.ticket_monitoring'
