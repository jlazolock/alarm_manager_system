import json
from contextlib import contextmanager
from typing import List

import pika

from apps.alarm_monitoring.models import ApplicationQueuesEnum


@contextmanager
def get_mq_channel():
    credentials = pika.PlainCredentials('guest', 'guest')
    connection = pika.BlockingConnection(pika.ConnectionParameters(
        host='172.17.0.2',
        port=5672,
        virtual_host='my_vhost',
        credentials=credentials
    ))
    try:
        channel = connection.channel()
        yield channel
    finally:
        connection.close()


def send_message(queue_name: ApplicationQueuesEnum, message: dict):
    with get_mq_channel() as (channel):
        channel.queue_declare(queue=queue_name.value)
        channel.basic_publish(
            exchange='',
            routing_key=queue_name.value,
            body=json.dumps(message)
        )


def send_messages(queue_name: ApplicationQueuesEnum, messages: List[dict]):
    with get_mq_channel() as (channel):
        channel.queue_declare(queue=queue_name.value)
        for message in messages:
            channel.basic_publish(
                exchange='',
                routing_key=queue_name.value,
                body=json.dumps(message)
            )


def read_message(queue_name: ApplicationQueuesEnum):
    with get_mq_channel() as (channel):
        channel.queue_declare(queue=queue_name.value)
        method_frame, header_frame, body = channel.basic_get(queue_name.value)
        if method_frame:
            channel.basic_ack(method_frame.delivery_tag)
            return json.loads(body)


def read_messages(queue_name: ApplicationQueuesEnum):
    with get_mq_channel() as (channel):
        channel.queue_declare(queue=queue_name.value)
        while (True):
            method_frame, header_frame, body = channel.basic_get(queue_name.value)
            if method_frame is None:
                break
            channel.basic_ack(method_frame.delivery_tag)
            yield json.loads(body)
